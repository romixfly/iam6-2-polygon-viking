﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class low_health : MonoBehaviour
{
    [FMODUnity.EventRef] public string snapevent;
    private FMOD.Studio.EventInstance snapInstance;
    public vThirdPersonController control;
    public bool SnapshotIsPlaying = false;
    public FMOD.Studio.PLAYBACK_STATE playback;

    // Start is called before the first frame update
    void Start()
    {
        snapInstance = FMODUnity.RuntimeManager.CreateInstance(snapevent);
    }

    // Update is called once per frame
    void Update()
    {
        /* Debug.Log(control.currentHealth);
         if(!SnapshotIsPlaying)
         {
             if (control.currentHealth <= 30)
             {
                 snapInstance = FMODUnity.RuntimeManager.CreateInstance(snapevent);
                 snapInstance.start();
                 SnapshotIsPlaying = true;
                 Debug.Log("Snapshot is playing");
             }
         }
         else
         {
             if (control.currentHealth > 30)
             {
                 snapInstance.stop (FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                 SnapshotIsPlaying = false;
                 Debug.Log("Snapshot stopped");
             }
         }*/
     
        //else snapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }

    public void HealthSnapshotstart()
    {
        snapInstance.getPlaybackState(out playback);
        if (playback != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            snapInstance.start();
            Debug.Log(playback);
        }
    }

    public void HealthSnapshotStop()
    {
        if (control.currentHealth > 30)
        {
            snapInstance.getPlaybackState(out playback);
            if (playback == FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
                snapInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
        }
    }
}
